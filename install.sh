#!/bin/bash

DIRNAME="py-kancolle-notification"
python3 -m venv "$DIRNAME"
cd "$DIRNAME"

git clone https://gitlab.com/kenzietandun/py-kancolle-notification.git src

source bin/activate

pip install -r src/req.txt

cp src/knotif.conf ~/.config/
cp src/knotif.db ~/.config/

INSTALL_DIR=$(pwd)

#mkdir -p ~/.config/systemd/user
#sed -i "s/INSTALL_DIR/${INSTALL_DIR}/" src/systemd/knotify.service
#cp src/systemd/knotify.service ~/.config/systemd/user/
#cp src/systemd/knotify.timer ~/.config/systemd/user/
#systemctl --user enable knotify.timer

if [[ -f ~/.bash_aliases ]]
then
    echo "alias kancolle='${INSTALL_DIR}/bin/python3 ${INSTALL_DIR}/src/main.py'" >> ~/.bash_aliases
elif [[ -f ~/.bashrc ]]
then
    echo "alias kancolle='${INSTALL_DIR}/bin/python3 ${INSTALL_DIR}/src/main.py'" >> ~/.bashrc
fi

echo "Setup finished. Open a new terminal and run kancolle --help for list of commands"
