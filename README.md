# py-kancolle-notification

Hourly Kancolle notification for Linux

### Why

Kancolle shipgirls are cute and I'd like to hear their voices 24/7/365.

This program also allows you to set your ship for the specific hour or randomise it every time it runs.

### Setup

##### Requirements

- Linux

- A working internet connection

- mpv

- systemd-timer

##### Manually

- Create a directory called `py-kancolle-notification` with `venv`, `python3 -m venv py-kancolle-notification`

- cd into the directory, `cd py-kancolle-notification`

- Clone the repository to `src` with `git clone https://gitlab.com/kenzietandun/py-kancolle-notification.git src`

- Activate your virtualenv, `source bin/activate`

- Install requirements, `pip install -r src/req.txt`

##### With provided script

- Run the provided `install.sh` with `curl https://gitlab.com/kenzietandun/py-kancolle-notification/install.sh | bash`

### Usage

- Go into the `src` directory and run `./main.py --help` to see all the available commands

- To make things more convenient you can put an alias in your `.bashrc` or `.bash_aliases` like so:

`alias kancolle='/path/to/virtualenv/bin/python3 /path/to/virtualenv/src/main.py`

- Then you can call the program with `kancolle --help` after re-sourcing your bash aliases file

##### Synchronising with kancolle wikia

- Initial database has been provided when you clone this repo, but it might be outdated

- First of all you need to sync local db with the kancolle wikia to get list of all available ships

`kancolle --sync`

- This might take a few minutes depending on your connection

##### List all available ships

- Run `kancolle --list`

##### Setting the default ship

- Run `kancolle --ship <SHIP>`

##### Setting the notification mode

- Notification mode can be set as `random`, `normal`, or `mixed`

- `random` randomises the ship that notifies the hour

- `normal` uses the default ship as the announcer

- `mixed` uses random ship as the announcer if the specified hour is not set to a specific ship

##### Setting working hour

- To set the working hour of your shipgirl, do `kancolle --time <start>-<end>`

- For example, `kancolle --time 6-22` will set the program to announce from 6am to 10pm

- To set it to work all the time, do `kancolle --time 0--23`

##### Setting volume

- Use `--volume` to set the notification volume, this volume is relative to your _current_ volume

- For example, `kancolle --volume 70` will set the notification volume to 70% of your current volume

##### Setting particular ship for particular time

- To set a ship for a particular time use `kancolle --hour <hour> --ship <ship>`

- For example, `kancolle --hour 11 --ship Yura` will set Yura as the announcer for 11am

- To use this feature, the notification mode must be set to `mixed`

### Test

- To test if everything is working corrently, run `kancolle --notify`

### Update

- Do a `git pull` on the repository directory

### Possible future improvements

- Feature requests are welcome
