#!/usr/bin/env python3

#---------------------------
# Written by Kenzie Tandun
# Nov 13, 2018
# nudnateiznek@gmail.com
#---------------------------

import sqlite3
from datetime import datetime
import os
from random import randint

class Db:
    def __init__(self, db_file):
        self.db_file = db_file
        if not os.path.isfile(db_file):
            os.mknod(db_file)
        self.conn = sqlite3.connect(db_file)
        self.c = self.conn.cursor()

        self.c.execute("""
        CREATE TABLE IF NOT EXISTS
        history 
        (name TEXT PRIMARY KEY,
         time TEXT)""")

        self.c.execute("""
        CREATE TABLE IF NOT EXISTS
        hourly_lines
        (ship_id TEXT PRIMARY KEY,
        ship_aliases TEXT,
        has_audio INT,
        audio_files TEXT)""")

        self.conn.commit()

    def get_last_timestamp(self):
        """ get the last time this program was last run """
        self.c.execute("""SELECT time FROM history
        WHERE name = 'last_check'""")

        result = self.c.fetchone()
        if result:
            return result[0]

        return None

    def write_timestamp(self):
        """ writes timestamp of when this program
        was last run """
        curr_time = datetime.now().strftime("%d%m%Y-%H%M%S")
        self.c.execute("""INSERT OR REPLACE INTO history
                VALUES ('last_check', (?))""", (curr_time,))
        self.conn.commit()

    def write_ship(self, ship_id, ship_aliases, has_audio, audio_files):
        """ given a ship_id string, 
        ship_aliases list of string, 
        has_audio boolean, 
        and audio_files list of string, 

        convert the data appropriate format to the database and
        write the data into the database 
        
        this function should also write the aliases of
        ship_id pointing towards ship_id to avoid writing
        the same data twice
        
        for example:
        Kongou should have its entry written as
        ship_id: Kongou
        ship_aliases: Kongou, Kongou Kai, Kongou Kai Ni
        has_audio: 1
        audio_files: [ *audio_files_url* ]

        Kongou Kai should have its entry written as
        ship_id: Kongou Kai
        ship_aliases: Kongou Kai
        has_audio: 0
        audio_files: None
        """

        ship_aliases_str = ','.join(ship_aliases)
        has_audio_int = 1 if has_audio else 0
        audio_files_str = "None"
        if audio_files:
            audio_files_str = ','.join(audio_files)

        self.c.execute("""
        INSERT OR IGNORE INTO hourly_lines
        VALUES
        ( (?), (?), (?), (?) )""", 
        (ship_id, ship_aliases_str, has_audio_int, audio_files_str))

        for ship_alias in ship_aliases[1:]:
            self.c.execute("""
            INSERT OR IGNORE INTO hourly_lines
            VALUES
            ( (?), (?), 0, 'None' )""", 
            (ship_alias, ship_id))

        self.conn.commit()

    def get_ship_list(self):
        """ Returns a list of ships that has_audio value of 1 """
        self.c.execute("""SELECT ship_id FROM hourly_lines
        WHERE has_audio = 1 ORDER BY ship_id ASC""")

        results = self.c.fetchall()
        return [r[0] for r in results]

    def get_ship_audio(self, ship_id, time):
        """ Returns audio URL that is associated with
        ship_id at <<time>>"""
        self.c.execute("""SELECT audio_files FROM hourly_lines
        WHERE ship_id = (?)""", (ship_id,))

        audio_str = self.c.fetchone()[0]
        audio_list = audio_str.split(',')
        return audio_list[int(time) % 24]

    def get_random_ship(self):
        """ Returns a random ship that has audio_files """
        self.c.execute("""SELECT ship_id FROM hourly_lines
        WHERE has_audio = 1""")

        results = self.c.fetchall()
        rand_ship_index = randint(0, len(results) - 1)
        return results[rand_ship_index][0]
