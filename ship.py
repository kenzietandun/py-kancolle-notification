#!/usr/bin/env python3

#---------------------------
# Written by Kenzie Tandun
# Nov 12, 2018
# nudnateiznek@gmail.com
#---------------------------

import requests
from bs4 import BeautifulSoup as bs

def get_ship_list():
    """ Returns a list of all ships' page url """

    wikia_root = "http://kancolle.fandom.com"
    ship_list_url = 'http://kancolle.fandom.com/wiki/Ship_List_(Image)'

    r = requests.get(ship_list_url)
    soup = bs(r.text, "lxml")

    ship_list = []

    ship_urls = soup.find("article", {"id": "WikiaMainContent"})
    for ship_url in ship_urls.find_all("a", 
            {"class": "image image-thumbnail link-internal"}, href=True):
        ship_page_url = ship_url.get("href")
        if "_Kai" not in ship_page_url:
            ship_list.append(wikia_root + ship_page_url)

    return ship_list

def parse_ship_data(url):
    """ given a ship's wikia page, 
    we parse its ship_id and check
    whether is has audio or not.

    Returns a tuple of ship_id and boolean
    of has_audio """

    r = requests.get(url)
    soup = bs(r.text, "lxml")

    ship_aliases = parse_ship_id_aliases(soup)
    has_audio = ship_has_audio(soup)

    audio_files = None
    if has_audio:
        audio_files = get_audio_files(soup)

    return (ship_aliases, has_audio, audio_files)

def parse_ship_id_aliases(soup):
    """ given a ship's soup
    parse its ship id and its aliases

    Returns list of ship_id and its aliases string

    for example:
    ["Kongou", "Kongou Kai", "Kongou Kai Ni"] """

    ship_aliases = []
    for ship_card in soup.find_all("table", 
            {"class": "typography-xl-optout infobox infobox-kai infobox-ship"}):
        for ship_name in ship_card.find_all("strong", {"class": "selflink"}):
            ship_aliases.append(ship_name.text)

    return ship_aliases

def ship_has_audio(soup):
    """ given a ship's soup
    check whether it has hourly notifications

    Returns boolean has_audio"""
    for h3 in soup.find_all("h3"):
        if "Hourly Notifications" in h3.text:
            return True

    return False

def get_audio_files(ship_soup):
    """ this function should only be called when
    a ship's has_audio is True, it then
    finds the ship's hourly lines and return their urls 
    
    Returns list of URLs for ship notification lines """

    hourly_lines = []

    i = 0
    for table in ship_soup.find_all("table", 
            {"class": "wikitable typography-xl-optout"}):
        # hourly lines are always in the second table
        if i == 1:
            for url in table.find_all("a", {"class": "internal"}, href=True):
                audio_url = url.get("href")
                if "New-" in audio_url or "KaiNi-" in audio_url:
                    print("Found newer line: {}".format(audio_url))
                    print("Deleting older line")
                    del hourly_lines[-1]

                hourly_lines.append(audio_url)
        i += 1

    return hourly_lines
