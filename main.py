#!/usr/bin/env python3

#---------------------------
# Written by Kenzie Tandun
# Nov 13, 2018
# nudnateiznek@gmail.com
#---------------------------

import os
import sys
import shutil
import argparse
import configparser
from db import Db
from ship import *
from datetime import datetime

home_dir = os.getenv("HOME")

def is_data_fresh(old_timestamp):
    """ checks if the old timestamp is at least a 
    week ago from current time 
    
    Returns Boolean"""

    curr_time = datetime.now()
    old_time = datetime.strptime(old_timestamp, "%d%m%Y-%H%M%S")

    time_diff = curr_time - old_time
    return time_diff.days <= 7

def sync(database):
    """ sync the database with latest information 
    from Kancolle wikia, the function only runs
    if the latest check was at least 7 days ago """

    old_timestamp = database.get_last_timestamp()

    if not old_timestamp or not is_data_fresh(old_timestamp):
        ship_list = get_ship_list()
        for ship_page in ship_list:
            ship_aliases, has_audio, audio_files = \
                parse_ship_data(ship_page)

            ship_id = ship_aliases[0]
            database.write_ship(ship_id, 
                    ship_aliases, 
                    has_audio, 
                    audio_files)
        database.write_timestamp()
    else:
        print("Data is still fresh, skipping sync.")
        sys.exit()

def notify(database, mode, ship, volume):
    """ checks the current mode and play the hourly notification 
    with the ship according to mode """
    if mode == "random":
        ship = database.get_random_ship()
    curr_time = datetime.now().strftime("%H")
    audio_url = database.get_ship_audio(ship, curr_time)
    os.system("mpv --volume={} '{}'".format(volume, audio_url))

def parse_arguments():
    """ Parses argument passed to main.py """
    db_file = home_dir + "/.config/knotif.db"
    dbase = Db(db_file)
    
    config_file = home_dir + "/.config/knotif.conf"

    cfg = configparser.ConfigParser()
    cfg.read(config_file)

    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--sync", help="Sync database with Kancolle Wikia",
            action="store_true")
    parser.add_argument("-r", "--random", help="Set hourly notification to random ships",
            action="store_true")
    parser.add_argument("-m", "--mixed", help="Set hourly notification to mixed ships",
            action="store_true")
    parser.add_argument("-n", "--normal", help="Set hourly notification to a ship",
            action="store_true")
    parser.add_argument("--ship", help="Set ship")
    parser.add_argument("--time", help="Set working time", default="6-22")
    parser.add_argument("--hour", help="Set hour", type=int)
    parser.add_argument("--volume", help="Set notification volume", type=int)
    parser.add_argument("--list", help="List ships", action="store_true")
    parser.add_argument("--notify", help="Play current hour's notification", 
            action="store_true")

    args = parser.parse_args()
    if args.notify:
        mode = cfg.get("conf", "mode")
        ship = cfg.get("conf", "ship")
        volume = cfg.get("conf", "volume")
        curr_time = datetime.now().strftime("%H")
        if mode == "mixed":
            try:
                hourly_ship = cfg.get("hourlies", curr_time)
                ship = hourly_ship
            except configparser.NoOptionError:
                mode = "random"
        start_time = int(cfg.get("conf", "start"))
        end_time = int(cfg.get("conf", "end"))
        curr_time_int = int(curr_time)
        if curr_time_int >= start_time and curr_time_int <= end_time:
            notify(dbase, mode, ship, volume)

    elif args.list:
        ships_list = dbase.get_ship_list()
        for index, ship in enumerate(ships_list):
            print("{:3d}. {}".format(index + 1, ship))
    elif args.volume:
        cfg["conf"]["volume"] = str(args.volume)
    elif args.sync:
        sync(dbase)
    elif args.random:
        print("Setting hourly notification to random ships")
        cfg["conf"]["mode"] = "random"
    elif args.mixed:
        print("Setting hourly notification to mixed ships")
        cfg["conf"]["mode"] = "mixed"
    elif args.normal:
        print("Setting hourly notification to a ship")
        cfg["conf"]["mode"] = "normal"
    elif args.hour:
        if args.ship:
            if args.hour < 1 or args.hour > 24:
                print("Hour must be between 1 and 24")
            else:
                cfg["hourlies"][str(args.hour)] = args.ship
        else:
                cfg["hourlies"][str(args.hour)] = ""
    elif args.ship:
        print("Setting ship to {}".format(args.ship))
        cfg["conf"]["ship"] = args.ship
    elif args.time:
        start, end = args.time.split('-')
        cfg["conf"]["start"] = start
        cfg["conf"]["end"] = end

    with open(config_file, 'w') as fil:
        cfg.write(fil)

def main():
    parse_arguments()

if __name__ == "__main__":
    main()
